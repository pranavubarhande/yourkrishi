/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { View, ScrollView, StyleSheet, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Colors } from 'react-native/Libraries/NewAppScreen';

import { getProductDetails } from '../../../services/productService';
import Product from '../../farmer/products/product';
import CustomButton from '../../../shared/CustomButton';
import { addOrder } from '../../../services/orderService';

const VendorHomeScreen = () => {
    const [productDetails, setProductDetails] = useState([]);
    const [orderAccepted, setOrderAccepted] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            getProductDetails().then((result) => {
                setProductDetails(result);
                const newArr = result.map(() => false);
                setOrderAccepted(newArr);
            });
        };

        fetchData();
    }, []);

    const renderProductDetails = () => {
        return productDetails.map((productDetail, index) => {
            const product = {
                id: productDetail.id,
                quantity: productDetail.quantity,
                price: productDetail.pricePerUnit,
                name: productDetail.productCategory.name,
                farmerId: productDetail.farmerId,
                image: productDetail.productCategory.imgUrl,
            };

            return (
                <Product key={product.id} product={product}>
                    {!orderAccepted[index] ? (
                        <>
                            <CustomButton
                                text="Request"
                                labelStyle={styles.btn}
                                onPress={() => onAccept(product, index)}
                                styleBtn={styles.styleBtn}
                                btnView={{alignItems: 'center'}}
                            />
                        </>
                    ) : (
                        <>
                            <CustomButton
                                text="Order Requested"
                                mode="default"
                                style={styles.orderAcceptedBtn}
                            />
                        </>
                    )}
                </Product>
            );
        });
    };

    const onAccept = async (product, index) => {
        const user = await AsyncStorage.getItem('user');
        const request = {
            farmerId: product.farmerId,
            productDetailsId: product.id,
            vendorId: JSON.parse(user).id,
            pickupLocation: null,
        };
        const result = await addOrder(request);
        console.log('The index', index);
        if (result.id) {
            setOrderAccepted((prevState) => {
                let arr = [...prevState];
                arr[index] = true;
                return arr;
            });
        }
    };
    const onDecline = (product, index) => {
        setOrderAccepted((prevState) => {
            let arr = [...prevState];
            arr[index] = false;
            return arr;
        });
    };

    return (
        <>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.text}>YourKrishi</Text>
                </View>

                <ScrollView style={styles.content}>
                    <Text style={styles.title}>Products Available</Text>
                    <ScrollView style={styles.scrollView}>
                        {renderProductDetails()}
                    </ScrollView>
                </ScrollView>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },
    header: {
        // backgroundColor: '#1976d2',
        backgroundColor: '#44ac5c',
        height: 100,
        paddingTop: 20,
        alignItems: 'center',
    },
    text: {
        color: Colors.white,
        fontSize: 30,
        fontStyle: 'italic',
        fontWeight: 'bold',
    },
    title: {
        color: Colors.dark,
        fontSize: 24,
        fontStyle: 'italic',
        fontWeight: 'bold',
        textAlign: 'center',
        paddingBottom: 10,
    },
    content: {
        padding: 20,
    },
    scrollView: {
        paddingBottom: 200,
    },
    orderAcceptedBtn: {
        paddingBottom: 10,
    },
    styleBtn: {
        width: 150,
        marginBottom: 15,
        backgroundColor: '#e64c51',
    },
});

export default VendorHomeScreen;
