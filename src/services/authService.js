/* eslint-disable prettier/prettier */
import client from '../utils/fetchUtil';
import { POST_METHOD, GET_METHOD } from './service.constants';

const SIGN_UP_URL = 'app/v1/users/sign-up';
const LOGIN_URL = 'login';
const USERS_URL = 'app/v1/users/user-info';
const VENDOR_INFO_URL = 'app/v1/users/vendor/user-info';
// localhost:8080/app/v1/users/vendor/user-info/?id=2
export const signUp = async (body) => {
    return await client(SIGN_UP_URL, POST_METHOD, body);
};

export const login = async (body) => {
    return await client(LOGIN_URL, POST_METHOD, body);
};

export const getUserInfo = async () => {
    return await client(USERS_URL, GET_METHOD);
};

export const getVendorInfo = async (id) => {
    return await client(`${VENDOR_INFO_URL}/?id=${id}`, GET_METHOD);
};
