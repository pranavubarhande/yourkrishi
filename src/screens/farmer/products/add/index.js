import {Picker} from '@react-native-picker/picker';
import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button, Dialog, Portal} from 'react-native-paper';

import CustomTextInput from '../../../../shared/CustomTextInput';
import {useTranslation} from 'react-i18next';

const AddProductCategory = ({onAddProduct, showAddProductDialog}) => {
    const {t} = useTranslation();
    const [product, setProduct] = useState('');
    const [productCategory, setProductCategory] = useState();

    return (
        <View>
            <Portal>
                <Dialog visible={showAddProductDialog}>
                    <Dialog.Title>{t('add_new_prod')}</Dialog.Title>
                    <Dialog.Content>
                        <CustomTextInput
                            value={product}
                            style={styles.customInput}
                            placeholder={t('product')}
                            onChange={(value) => setProduct(value)}
                        />
                        <View>
                            <Picker
                                selectedValue={productCategory}
                                mode="dropdown"
                                onValueChange={(value) =>
                                    setProductCategory(value)
                                }>
                                <Picker.Item
                                    label={t('select_category')}
                                    value=""
                                />
                                <Picker.Item
                                    label={t('fruits')}
                                    value="fruits"
                                />
                                <Picker.Item
                                    label={t('veg')}
                                    value="vegetables"
                                />
                                <Picker.Item
                                    label={t('cereals')}
                                    value="cereals"
                                />
                                <Picker.Item
                                    label={t('pulses')}
                                    value="pulses"
                                />
                            </Picker>
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button
                            onPress={() =>
                                onAddProduct('cancel', product, productCategory)
                            }>
                            {t('cancel')}
                        </Button>
                        <Button
                            onPress={() =>
                                onAddProduct('ok', product, productCategory)
                            }>
                            {t('ok')}
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View>
    );
};

const styles = StyleSheet.create({
    customInput: {
        marginBottom: 20,
    },
});

export default AddProductCategory;
