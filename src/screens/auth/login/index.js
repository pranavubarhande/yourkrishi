import React, {useState, useEffect} from 'react';
import {SafeAreaView, Text, View, TouchableOpacity, Button} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CustomTextInput from '../../../shared/CustomTextInput';
import CustomButton from '../../../shared/CustomButton';
import styles from '../../../styles/login';
import inputStyles from '../../../styles/input';
import brandStyles from '../../../styles/brand';
import {getUserInfo, login} from '../../../services/authService';
import {AuthContext} from '../../../context/AuthContext';
import i18n from 'i18next';
import {useTranslation} from 'react-i18next';
import {Picker} from '@react-native-picker/picker';
import {useFocusEffect} from '@react-navigation/native';

const LoginScreen = ({navigation}) => {
    const [contactNumber, setContactNumber] = useState();
    const [password, setPassword] = useState();
    const {signIn} = React.useContext(AuthContext);
    const {t} = useTranslation();
    const onLogin = () => {
        if (!contactNumber || !password) {
            alert(t('alert_fill_details'));
            return;
        }

        const request = {
            mobileNo: contactNumber,
            password,
        };

        login(request)
            .then((response) => {
                const authToken = response.headers.get('authorization');
                AsyncStorage.setItem('user_token', authToken);

                getUserInfo()
                    .then((user) => {
                        AsyncStorage.setItem('user', JSON.stringify(user));
                        signIn(user);
                    })
                    .catch(() => {});
            })
            .catch(() => alert(t('alert_invalid_cred')));
    };

    const [lang, setLang] = useState('en');

    const getLang = async () => {
        try {
            const user_lang = await AsyncStorage.getItem('user_lang');
            console.log('here ->', user_lang);
            if (user_lang !== null) {
                setLang(user_lang);
            }
        } catch (e) {
            console.log('Error', e);
        }
    };

    useFocusEffect(() => {
        getLang();
    }, [lang]);

    return (
        <SafeAreaView style={styles.container}>
            <Text style={brandStyles.brand}>YourKrishi</Text>

            <CustomTextInput
                value={contactNumber}
                style={inputStyles.input}
                placeholder={t('contact_number')}
                keyboardType="numeric"
                onChange={(value) => setContactNumber(value)}
            />

            <CustomTextInput
                value={password}
                style={inputStyles.input}
                placeholder={t('password')}
                secureTextEntry={true}
                onChange={(value) => setPassword(value)}
            />

            <CustomButton
                text={t('login')}
                onPress={onLogin}
                styleBtn={{backgroundColor: '#346856'}}
            />

            <View style={styles.createAccountWrapper}>
                <Text style={styles.newAccountText}>
                    {t('create_new_account')}?
                </Text>

                <TouchableOpacity
                    onPress={() => navigation.navigate('Register')}>
                    <Text style={styles.signUp}>{t('sign_up')}</Text>
                </TouchableOpacity>
            </View>

            <View style={{marginTop: 10}}>
                <Picker
                    selectedValue={lang}
                    mode="dropdown"
                    style={inputStyles.picker}
                    onValueChange={(value) => {
                        setLang(value);
                        i18n.changeLanguage(value);
                        AsyncStorage.setItem('user_lang', value);
                    }}>
                    <Picker.Item label="English" value="en" />
                    <Picker.Item label="Hindi" value="hi" />
                </Picker>
            </View>
        </SafeAreaView>
    );
};

export default LoginScreen;
