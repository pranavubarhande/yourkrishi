import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import VendorHomeScreen from '../vendor/home';
import OrdersScreen from '../vendor/orders';
import Logout from '../auth/logout';
import {useTranslation} from 'react-i18next';

const Tab = createMaterialBottomTabNavigator();

const VendorFlowNavigator = () => {
    const {t} = useTranslation();
    return (
        <Tab.Navigator
            initialRouteName="VendorHome"
            headerMode="none"
            barStyle={{backgroundColor: '#44ac5c'}}
            activeColor="#ffffff">
            <Tab.Screen
                name="VendorHome"
                component={VendorHomeScreen}
                options={{
                    tabBarLabel: t('home'),
                    tabBarIcon: ({color}) => (
                        <MaterialCommunityIcons
                            name="home"
                            color={color}
                            size={26}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Orders"
                component={OrdersScreen}
                options={{
                    tabBarLabel: t('order'),
                    tabBarIcon: ({color}) => (
                        <MaterialCommunityIcons
                            name="cart"
                            color={color}
                            size={26}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Logout"
                component={Logout}
                options={{
                    tabBarLabel: t('logout'),
                    tabBarIcon: ({color}) => (
                        <MaterialCommunityIcons
                            name="logout"
                            color={color}
                            size={26}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

export default VendorFlowNavigator;
