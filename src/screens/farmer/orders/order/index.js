/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { Card, Divider, Title, Paragraph, Button } from 'react-native-paper';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { acceptOrderForFarmer, declineOrderForFarmer } from '../../../../services/orderService';
import CustomButton from '../../../../shared/CustomButton';
import Form from '../locationForm/Form';
import Geolocation from '@react-native-community/geolocation';
import { getVendorInfo } from '../../../../services/authService';
import { getFarmerLocation } from '../../../../services/orderService';
import {useTranslation} from 'react-i18next';

const Order = ({ order }) => {
    const [showAddProductDialog, setShowAddProductDialog] = useState(false);
    const [vendorInfo, setVendorInfo] = useState({});

    const shareLocation = () => {
        Geolocation.getCurrentPosition(data => console.warn(data.coords));
    };

    const {t} = useTranslation();
    const [accept, setAccept] = useState(order.isAccepted);
    const acceptOrder = async () => {
        const result = await acceptOrderForFarmer(order.id);
        setAccept(true);
        console.log('The result', result);
    };

    const declineOrder = async () => {
        const result = await declineOrderForFarmer(order.id);
        setAccept(false);
        console.log('The result->', result);
    };

    useEffect(() => {
        const fetchData = async () => {
            const res = await getVendorInfo(order.vendorId);
            setVendorInfo(res);
            console.warn(res);
        };
        fetchData();
    }, [order.vendorId]);

    const onAddLocation = (action, InputFields) => {
        if (action === 'cancel') {
            setShowAddProductDialog(false);
            return;
        }

        if (!InputFields.field1 || !InputFields.field2 || !InputFields.field3 || !InputFields.field4) {
            alert(t('alert_fill_details'));
            return;
        }
    };
    useEffect(() => {
        const getFarmerCurrentLocation = async () => {
            const request = {
                orderId: order.id,
                latitude: 26.8,
                longitude: 86.2,
                altitude: 0,
            };
            const result = await getFarmerLocation(request);
            console.warn(result.pickupLocation);
            // result.pickupLocation.latitude =
            // result.pickupLocation.longitude =
        };
        getFarmerCurrentLocation();
    }, [order.id]);
    // const payload = {
    //     type: productCategory,
    //     name: product,
    // };

    // addProductCategory(payload).then(() => setShowAddProductDialog(false));
    // here api shall be used instead of addProductCategory

    const onAddAddress = () => {
        setShowAddProductDialog(true);
    };

    return (
        <Card style={styles.container}>
            <Card.Content>
                <Title style={styles.name}>{order.name}</Title>
                <Divider />
                <View>
                    <View style={styles.productContainer}>
                        <Image
                            style={styles.image}
                            source={{ uri: order.image }}
                        />
                        <View>
                            <View style={styles.product}>
                                <Paragraph style={styles.productInfo}>Price: <Text style={styles.bold}>₹{order.price}/Kg</Text></Paragraph>
                                <Paragraph style={styles.productInfo}>Quantity: <Text style={styles.bold}>{order.quantity} Kg</Text></Paragraph>
                            </View>
                            <View style={styles.vendor}>
                                <Paragraph style={styles.vendorInfoStyle}> <FontAwesome5 name={'user'} style={styles.icon} /> <Text style={styles.bold}>{vendorInfo.name}</Text></Paragraph>
                                <Paragraph style={styles.vendorInfoStyle}>
                                    <FontAwesome5 name={'star'} style={styles.icon} />
                                    <FontAwesome5 name={'star'} style={styles.icon} />
                                    <FontAwesome5 name={'star'} style={styles.icon} />
                                    <FontAwesome5 name={'star'} style={styles.icon} />
                                    <FontAwesome5 name={'star'} style={styles.icon} />
                                    <Text style={styles.bold}>{vendorInfo.rating}</Text></Paragraph>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.deliveryContainer}>
                    <View style={styles.deliveryStatus}>
                        {
                            accept !== true && accept !== false ? <>
                            <Button
                                style={styles.btn}
                                mode="outlined"
                                color="green"
                                onPress={() => acceptOrder(order)}>
                                {t('accept')}
                            </Button>
                            <Button
                                style={styles.btn}
                                mode="outlined"
                                color="red"
                                onPress={() => declineOrder(order)}>
                                {t('decline')}
                            </Button>
                        </>
                        :
                            null
                        }
                    </View>
                        {
                            accept && <View style={styles.orderContainer}><Text style={styles.orderAccepted}>{t('order_accepted')} !</Text></View>
                        }
                        {
                            accept && <CustomButton
                                        mode="outlined"
                                        text={t('share_location')}
                                        onPress={shareLocation}
                                        disabled={true}
                                      />
                        }
                        {
                            accept && <CustomButton
                                            mode="outlined"
                                            text={t('add_address')}
                                            onPress={onAddAddress}
                                        />
                        }
                        <Form
                            showAddProductDialog={showAddProductDialog}
                            onAddLocation={onAddLocation}
                        />
                </View>
            </Card.Content>
        </Card>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        marginBottom: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#E5E7EB',
    },
    name: {
        fontSize: 24,
        textAlign: 'center',
        paddingBottom: 10,
        textTransform: 'capitalize',
        fontWeight: 'bold',
        letterSpacing: 4,
    },
    info: {
        marginTop: 10,
        fontSize: 18,
        textAlign: 'center',
    },
    productContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    bold: {
        fontWeight: 'bold',
    },
    vendor: {
        paddingTop: 10,
        paddingBottom: 10,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    vendorInfo: {
        fontSize: 18,
        letterSpacing: 3,
    },
    icon: {
        fontSize: 18,
        paddingTop: 10,
    },
    image: {
        height: 120,
        width: '45%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#E5E7EB',
    },
    product: {
        display: 'flex',
        flexDirection: 'column',
    },
    productInfo: {
        fontSize: 20,
        marginTop: 8,
        letterSpacing: 2,
    },
    vendorInfoStyle: {
        fontSize: 18,
        marginTop: 10,
        zIndex: 5,
        paddingTop: 2,
    },
    deliveryContainer: {
        alignItems: 'center',
    },
    deliveryStatus: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 20,
        fontSize: 18,
        textAlign: 'center',
        color: 'green',
    },
    btn: {
        width: '50%',
    },
    orderAccepted: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        color: '#44ac5c',
        textTransform: 'uppercase',
        letterSpacing: 1,
    },
    btnContainer: {
        display: 'flex',
        flexDirection: 'row',
    },
});

export default Order;
