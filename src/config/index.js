import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import AsyncStorage from '@react-native-community/async-storage';
import translationEN from './en/translation.json';
import translationHI from './hi/translation.json';
const fallbackLng = ['en'];
const availableLanguages = ['en', 'hi'];
const resources = {
    en: {
        translation: translationEN,
    },
    hi: {
        translation: translationHI,
    },
};
let langauge = 'hi';
const getLang = async () => {
    try {
        const user_lang = await AsyncStorage.getItem('user_lang');
        if (user_lang !== null) {
            langauge = user_lang;
            // console.log({user_lang});
        }
        // console.log({langauge});
    } catch (e) {
        console.log('Error', e);
    } finally {
        i18n.use(initReactI18next).init({
            resources,
            lng: langauge,
            fallbackLng,
            whitelist: availableLanguages,
            interpolation: {escapeValue: false},
        });
    }
};

getLang();

export default i18n;
