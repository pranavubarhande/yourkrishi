import React, {useState, useEffect} from 'react';
import {format} from 'date-fns';
import {faPhoneAlt, faLock} from '@fortawesome/free-solid-svg-icons';
import {faUser} from '@fortawesome/free-regular-svg-icons';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import i18n from 'i18next';
import AsyncStorage from '@react-native-community/async-storage';
import CustomDatePicker from '../../../shared/CustomDatePicker';
import CustomTextInput from '../../../shared/CustomTextInput';
import CustomButton from '../../../shared/CustomButton';
import {signUp} from '../../../services/authService';
import styles from '../../../styles/register';
import inputStyles from '../../../styles/input';
import brandStyles from '../../../styles/brand';
import {useTranslation} from 'react-i18next';

const RegisterScreen = ({navigation}) => {
    const {t} = useTranslation();
    const [username, setUsername] = useState();
    const [contactNumber, setContactNumber] = useState();
    const [dob, setDobInInput] = useState(new Date());
    const [role, setRole] = useState('farmer');
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [showDatePicker, setShowDatePicker] = useState(false);
    const [lang, setLang] = useState('en');

    useEffect(() => {
        getLang();
    }, [lang]);

    const onSignUp = () => {
        if (!username || !contactNumber || !dob || !role || !password) {
            alert(t('alert_fill_details'));
            return;
        }

        const request = {
            name: username,
            mobileNo: contactNumber,
            password,
            dob: format(dob, 'yyyy-MM-dd'),
            role,
            language: lang,
        };

        signUp(request)
            .then(() => navigation.navigate('Login'))
            .catch((err) => console.error(err));
    };

    const getLang = async () => {
        try {
            const user_lang = await AsyncStorage.getItem('user_lang');
            if (user_lang !== null) {
                setLang(user_lang);
            }
        } catch (e) {
            console.log('Error', e);
        }
    };

    return (
        <ScrollView contentContainerStyle={styles.container}>
            <Text style={brandStyles.brand}>YourKrishi</Text>

            <CustomTextInput
                value={username}
                style={inputStyles.input}
                icon={faUser}
                // placeholder="Username"
                placeholder={t('username')}
                onChange={(value) => setUsername(value)}
            />

            <CustomTextInput
                value={contactNumber}
                style={inputStyles.input}
                icon={faPhoneAlt}
                placeholder={t('contact_number')}
                keyboardType="numeric"
                onChange={(value) => setContactNumber(value)}
            />

            <View style={inputStyles.dateInput}>
                <CustomTextInput
                    placeholder="DD/MM/YYYY"
                    style={inputStyles.input}
                    onFocus={() => setShowDatePicker(true)}
                    value={format(dob, 'dd-MM-yyyy')}
                />
                <CustomDatePicker
                    show={showDatePicker}
                    setDateInInput={(value) => setDobInInput(value)}
                    setShow={(value) => setShowDatePicker(value)}
                />
            </View>

            <View>
                <Picker
                    selectedValue={role}
                    mode="dropdown"
                    style={inputStyles.picker}
                    onValueChange={(value) => setRole(value)}>
                    <Picker.Item label={t('select_role')} value="" />
                    <Picker.Item label={t('farmer')} value="farmer" />
                    <Picker.Item label={t('vendor')} value="vendor" />
                </Picker>
            </View>

            <CustomTextInput
                value={password}
                style={inputStyles.input}
                icon={faLock}
                placeholder={t('password')}
                secureTextEntry={true}
                onChange={(value) => setPassword(value)}
            />

            <CustomTextInput
                value={confirmPassword}
                style={inputStyles.input}
                icon={faLock}
                placeholder={t('confirm_password')}
                secureTextEntry={true}
                onChange={(value) => setConfirmPassword(value)}
            />

            <CustomButton
                mode="contained"
                text={t('sign_up')}
                onPress={() => onSignUp()}
                styleBtn={{backgroundColor: '#346856'}}
            />

            <View style={{marginTop: 10}}>
                <Picker
                    selectedValue={lang}
                    mode="dropdown"
                    style={inputStyles.picker}
                    onValueChange={(value) => {
                        setLang(value);
                        i18n.changeLanguage(value);
                        AsyncStorage.setItem('user_lang', value);
                    }}>
                    <Picker.Item label="English" value="en" />
                    <Picker.Item label="Hindi" value="hi" />
                </Picker>
            </View>

            <View style={styles.createAccountWrapper}>
                <Text style={styles.newAccountText}>
                    {t('already_have_an_account')}?
                </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.login}>{t('login')}</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

export default RegisterScreen;
