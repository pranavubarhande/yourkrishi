import {Picker} from '@react-native-picker/picker';
import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';
import {useTranslation} from 'react-i18next';
import CustomTextInput from '../../../../shared/CustomTextInput';

const AddComponent = ({onAddProduct}) => {
    const [product, setProduct] = useState('');
    const [productCategory, setProductCategory] = useState();
    const {t} = useTranslation();

    return (
        <View style={styles.trialStyle}>
            <View>
                <CustomTextInput
                    value={product}
                    style={styles.textInput}
                    placeholder={t('product')}
                    onChange={(value) => setProduct(value)}
                />
                <View>
                    <Picker
                        selectedValue={productCategory}
                        mode="dropdown"
                        onValueChange={(value) => setProductCategory(value)}>
                        <Picker.Item label={t('select_category')} value="" />
                        <Picker.Item label={t('fruits')} value="fruits" />
                        <Picker.Item label={t('veg')} value="vegetables" />
                        <Picker.Item label={t('cereals')} value="cereals" />
                        <Picker.Item label={t('pulses')} value="pulses" />
                    </Picker>
                </View>
                <View style={styles.btnWrapper}>
                    <Button
                        style={styles.btn}
                        onPress={() =>
                            onAddProduct('ok', product, productCategory)
                        }
                        mode="outlined"
                        color="green">
                        {t('add')}
                    </Button>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    trialStyle: {
        marginLeft: '12%',
        width: '77%',
    },
    btn: {
        marginTop: 10,
        width: 100,
    },
    btnWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15,
    },
    textInput: {
        marginBottom: 20,
    },
});

export default AddComponent;
