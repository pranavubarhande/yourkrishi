import {StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export const styles = StyleSheet.create({
    brand: {
        fontSize: 36,
        fontWeight: 'bold',
        color: '#346856',
        textAlign: 'center',
        marginBottom: 30,
    },
});

export default styles;
