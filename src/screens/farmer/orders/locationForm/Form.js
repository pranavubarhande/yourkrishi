/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Button, Dialog, Portal, TextInput } from 'react-native-paper';

const Form = ({ onAddLocation, showAddProductDialog }) => {
    const [Input, setInput] = React.useState({
        field1: '',
        field2: '',
        field3: '',
        field4: '',
    });

    return (
        <View style={styles.trialStyle}>
            <Portal>
                <Dialog visible={showAddProductDialog}>
                    <Dialog.Title style={styles.header}>Add Address</Dialog.Title>
                    <TextInput
                        label="House No."
                        value={Input.field1}
                        onChangeText={(inputText) =>
                            setInput((prevState) => {
                                return {
                                    ...prevState,
                                    field1: inputText,
                                };
                            })
                        }
                        style={styles.inputFields}
                    />
                    <TextInput
                        label="Street No."
                        value={Input.field2}
                        onChangeText={(inputText) =>
                            setInput((prevState) => {
                                return {
                                    ...prevState,
                                    field2: inputText,
                                };
                            })
                        }
                        style={styles.inputFields}
                    />
                    <View style={styles.formFlex}>
                        <TextInput
                            label="City"
                            value={Input.field3}
                            onChangeText={(inputText) =>
                                setInput((prevState) => {
                                    return {
                                        ...prevState,
                                        field3: inputText,
                                    };
                                })
                            }
                            style={[styles.inputFields, styles.formFlexInfo]}
                        />
                        <TextInput
                            label="Pin Code"
                            value={Input.field3}
                            onChangeText={(inputText) =>
                                setInput((prevState) => {
                                    return {
                                        ...prevState,
                                        field3: inputText,
                                    };
                                })
                            }
                            style={[styles.inputFields, styles.formFlexInfo]}
                        />
                    </View>
                    <TextInput
                        label="State"
                        value={Input.field4}
                        onChangeText={(inputText) =>
                            setInput((prevState) => {
                                return {
                                    ...prevState,
                                    field4: inputText,
                                };
                            })
                        }
                        style={styles.inputFields}
                    />
                    <Dialog.Actions>
                        <Button
                            onPress={() => onAddLocation('cancel', Input)}>
                            Cancel
                        </Button>
                        <Button
                            onPress={() => onAddLocation('ok', Input)}>
                            Ok
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View>
    );
};

const styles = StyleSheet.create({
    trialStyle: {
        marginTop: 20,
        width: '100%',
        borderColor: '#fff',
    },
    header: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 10,
    },
    btn: {
        marginTop: 10,
        marginBottom: 10,
    },
    formFlex: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 18,
    },
    formFlexInfo: {
        width: '45%',
    },
    inputFields: {
        marginBottom: 15,
        width: '90%',
        marginLeft: '5%',
        borderRadius: 5,
        backgroundColor: '#E5E7EB',
        fontWeight: 'bold',
        fontSize: 18,
    },
});

export default Form;
