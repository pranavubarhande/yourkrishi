/* eslint-disable prettier/prettier */
import client from '../utils/fetchUtil';
import { GET_METHOD, POST_METHOD, PUT_METHOD } from './service.constants';

export const PRODUCT_CATEGORIES_URL = 'app/v1/orderCategory';
export const ORDER_DETAILS_URL = 'app/v1/orders';
export const ORDER_ACCEPT_URL = 'app/v1/orders/accept';
export const ORDER_DECLINE_URL = 'app/v1/orders/decline';
export const FARMER_LOCATION_URL = 'app/v1/orders/location';

export const getOrderCategories = async () => {
    return await client(PRODUCT_CATEGORIES_URL, GET_METHOD);
};

export const getOrderDetails = async () => {
    return await client(ORDER_DETAILS_URL, GET_METHOD);
};

export const getOrderDetailsForFarmer = async (id) => {
    return await client(`${ORDER_DETAILS_URL}/farmer?id=${id}`, GET_METHOD);
};

export const getOrderDetailsForVendor = async (id) => {
    return await client(`${ORDER_DETAILS_URL}/vendor?id=${id}`, GET_METHOD);
};

export const acceptOrderForFarmer = async (id) => {
    return await client(`${ORDER_ACCEPT_URL}?id=${id}`, PUT_METHOD);
};

export const declineOrderForFarmer = async (id) => {
    return await client(`${ORDER_DECLINE_URL}?id=${id}`, PUT_METHOD);
};

export const addOrder = async (body) => {
    return await client(ORDER_DETAILS_URL, POST_METHOD, body);
};

export const addProductCategory = async (body) => {
    return await client(PRODUCT_CATEGORIES_URL, POST_METHOD, body);
};

export const getFarmerLocation = async (body) => {
    return await client(FARMER_LOCATION_URL, PUT_METHOD, body);
};
