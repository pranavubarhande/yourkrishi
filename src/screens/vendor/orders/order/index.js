/* eslint-disable prettier/prettier */
import React from 'react';
import {Linking, StyleSheet, Text, View, Image} from 'react-native';
import {Card, Divider, Title, Paragraph} from 'react-native-paper';
import CustomButton from '../../../../shared/CustomButton';
import {getUserInfo} from '../../../../services/authService';
import openMap from 'react-native-open-maps';
import {createOpenLink, createMapLink} from 'react-native-open-maps';

const Order = ({ order }) => {
    console.log({ order });
    const shareLocation = async () => {
        console.log({ order });
        // TODO: need an api to get the user info with id to share whatsapp location
        const user = await getUserInfo(order.farmerId);

        const whatsAppMsg = `Location details for the order 
        ${order.name}
        ${order.price}
        ${order.quantity}
        `;
        const mobileNumber = user.mobileNo;

        console.log({ user, mobileNumber, whatsAppMsg });

        let url =
            'whatsapp://send?text=' + whatsAppMsg + '&phone=91' + mobileNumber;
        Linking.openURL(url)
            .then(() => {
                console.log('Success');
            })
            .catch(() => {
                alert(
                    'Make sure Whatsapp is installed on your device to share location',
                );
            });
    };

    // const gotoAdd = () => {
    //     openMap({
    //         latitude: parseFloat(order.pickupLocation.latitide),
    //         longitude: parseFloat(order.pickupLocation.longitude),
    //     });
    // };
    const destination = {
        // latitude: parseFloat(order.pickupLocation.latitide),
        // longitude: parseFloat(order.pickupLocation.longitude),
        zoom: 16,
    };

    if (order.pickupLocation !== null) {
        destination.latitude = parseFloat(order.pickupLocation.latitide);
        destination.longitude = parseFloat(order.pickupLocation.longitude);
    }

    return (
        <Card style={styles.container}>
            <Card.Content>
                <Title style={styles.name}>{order.name}</Title>
                <Divider />
                <View style={styles.order}>
                    <Image
                        style={styles.image}
                        source={{ uri: order.image }}
                    />
                    <View>
                        <Paragraph style={styles.info}>Price: ₹{order.price}</Paragraph>
                        <Paragraph style={styles.info}>
                            Quantity: {order.quantity} Kg
                        </Paragraph>
                    </View>
                </View>
                <View>
                    {order.isAccepted ? (
                        <Text
                            style={{
                                ...styles.orderStatus,
                                ...styles.orderAccepted,
                            }}>
                            Order Status: &nbsp; Order Accepted
                        </Text>
                    ) : null}
                    {order.isAccepted === null ? (
                        <Text
                            style={{
                                ...styles.orderStatus,
                                ...styles.orderProgress,
                            }}>
                            Order Status: &nbsp; Order in Progress
                        </Text>
                    ) : null}
                    {order.isAccepted === false ? (
                        <Text
                            style={{
                                ...styles.orderStatus,
                                ...styles.orderRejected,
                            }}>
                            Order Status: &nbsp; Order Rejected
                        </Text>
                    ) : null}
                </View>
                <CustomButton
                    text="Go to Address"
                    onPress={createOpenLink(destination)}
                    styleBtn={{
                        ...styles.styleBtn,
                        backgroundColor: order.pickupLocation === null ? '#CCCCCC':'#e64c51'}}
                    btnView={{alignItems: 'center'}}
                    disabled={order.pickupLocation === null ? true : false}
                    labelStyle={styles.labelStyle}
                />
            </Card.Content>
        </Card>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        marginBottom: 20,
    },
    name: {
        fontSize: 24,
        textAlign: 'center',
        paddingBottom: 10,
        textTransform: 'capitalize',
    },
    order: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    info: {
        marginTop: 10,
        fontSize: 18,
    },
    image: {
        height: 100,
        width: 180,
        borderColor: 'black',
    },
    orderStatus: {
        fontSize: 17,
        color: 'black',
        marginTop: 15,
        textAlign: 'center',
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    orderAccepted: {
        color: 'green',
    },
    orderRejected: {
        color: 'red',
    },
    orderProgress: {
        color: '#F7CA02',
    },
    styleBtn: {
        width: 200,
        marginBottom: 15,
    },
    labelStyle: {
        color: '#fff',
    },
});

export default Order;
