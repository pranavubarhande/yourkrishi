import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, View, ScrollView} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Picker} from '@react-native-picker/picker';
import {faRupeeSign, faWeight} from '@fortawesome/free-solid-svg-icons';

import {CustomTextInput} from '../../../../shared/CustomTextInput';
import {CustomButton} from '../../../../shared/CustomButton';
import {getProductCategories} from '../../../../services/productService';
import inputStyles from '../../../../styles/input';
import AddComponent from '../add/AddComponent';
import {addProductCategory} from '../../../../services/productService';
import {useTranslation} from 'react-i18next';
import {List} from 'react-native-paper';
import AddProductCategory from '../add';

export const SellProduct = ({navigation}) => {
    const {t} = useTranslation();
    const [item, setItem] = useState();
    const [price, setPrice] = useState();
    const [quantity, setQuantity] = useState();
    const [productCategories, setProductCategories] = useState([]);
    const [showAddProductDialog, setShowAddProductDialog] = useState(false);

    useEffect(() => {
        getProductCategories().then((result) => setProductCategories(result));
    }, []);

    const onNext = () => {
        if (!item || !quantity || !price) {
            alert(t('alert_fill_details'));
            return;
        }
        const product = {
            item,
            quantity,
            price,
            id: productCategories.find((product) => product.name === item).id,
        };
        navigation.navigate('Edit', {product});
    };

    const onCancel = () => {
        navigation.navigate('Products');
    };

    const onAddProduct = (action, product, productCategory) => {
        if (action === 'cancel') {
            setShowAddProductDialog(false);
            return;
        }

        if (!productCategory || !product) {
            // ß;
            alert(t('alert_fill_details'));
            return;
        }

        const payload = {
            type: productCategory,
            name: product,
        };

        addProductCategory(payload).then(() => setShowAddProductDialog(false));
    };

    const [expanded, setExpanded] = React.useState(false);
    const handlePress = () => setExpanded((prevState) => !prevState);

    return (
        <ScrollView style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.text}>{t('upload_items')}</Text>
            </View>

            <View style={styles.content}>
                <View style={styles.inputWrapper}>
                    <View style={styles.addItem}>
                        <Text style={styles.addItemText}>Select the products here to sell</Text>
                    </View>
                    <View style={inputStyles.input}>
                        <Picker
                            selectedValue={item}
                            mode="dropdown"
                            style={inputStyles.picker}
                            onValueChange={(value) => setItem(value)}>
                            <Picker.Item label={t('select_product')} value="" />
                            {productCategories.map((product) => (
                                <Picker.Item
                                    key={product.id}
                                    label={product.name}
                                    value={product.name}
                                />
                            ))}
                        </Picker>
                    </View>

                    <View>
                        <CustomTextInput
                            value={price}
                            style={inputStyles.input}
                            icon={faRupeeSign}
                            placeholder={t('price_per_kg') + t('in_INR')}
                            keyboardType="numeric"
                            onChange={(value) => setPrice(value)}
                        />
                    </View>

                    <View>
                        <CustomTextInput
                            value={quantity}
                            style={inputStyles.input}
                            icon={faWeight}
                            placeholder={t('quantity') + t('in_kg')}
                            keyboardType="numeric"
                            onChange={(value) => setQuantity(value)}
                        />
                    </View>
                </View>

                <View style={styles.buttonsWrapper}>
                    <View style={styles.nextButton}>
                        <CustomButton
                            text={t('next')}
                            onPress={() => onNext()}
                            styleBtn={styles.styleBtn}
                        />
                    </View>
                    <View style={styles.cancelButton}>
                        <CustomButton
                            text={t('cancel')}
                            onPress={() => onCancel()}
                            styleBtn={styles.styleBtn}
                        />
                    </View>
                </View>

                <AddProductCategory
                    showAddProductDialog={showAddProductDialog}
                    onAddProduct={onAddProduct}
                />
                <View style={styles.accordionWrapper}>
                    <View style={styles.addItem}>
                        <Text style={styles.addNewItemTextHeader}>
                            {t('unable_to_find')}
                        </Text>
                        <Text style={styles.addNewItemText}>
                            {t('add_new_product_here')}
                        </Text>
                    </View>

                    <List.AccordionGroup>
                        <List.Accordion
                            id="1"
                            title={t('add_new_prod')}
                            expanded={expanded}
                            onPress={handlePress}
                            theme={{
                                colors: {
                                    primary: 'black',
                                },
                            }}
                            style={styles.accordionStyle}>
                            <AddComponent onAddProduct={onAddProduct} />
                        </List.Accordion>
                    </List.AccordionGroup>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
    },

    header: {
        backgroundColor: '#44ac5c',
        height: 100,
        paddingTop: 20,
        alignItems: 'center',
    },
    addItemText: {
        fontSize: 18,
        paddingTop: 15,
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    addNewItemTextHeader: {
        fontSize: 18,
        marginBottom: 5,
        marginTop: 25,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    addNewItemText: {
        fontSize: 18,
        marginBottom: 10,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    text: {
        color: Colors.white,
        fontSize: 30,
        fontStyle: 'italic',
        fontWeight: 'bold',
    },
    content: {
        padding: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonsWrapper: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    nextButton: {
        marginRight: 30,
    },
    accordionWrapper: {
        width: '100%',
        marginTop: 10,
        backgroundColor: '#d4cfcf',
    },
    inputWrapper: {
        width: '100%',
        marginTop: 10,
        backgroundColor: '#d4cfcf',
        alignItems: 'center',
    },
    accordionStyle: {
        marginTop: 0,
        marginBottom: 10,
        width: '80%',
        marginLeft: '10%',
    },
    styleBtn: {
        backgroundColor: '#e64c51',
    },
});

export default SellProduct;
