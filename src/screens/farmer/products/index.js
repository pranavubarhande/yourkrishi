import React, {useState} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import {FAB, Button, List, Card} from 'react-native-paper';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {addProductCategory} from '../../../services/productService';
import AddProductCategory from './add';
import AddComponent from './add/AddComponent';
import {useTranslation} from 'react-i18next';
import {CustomButton} from '../../../shared/CustomButton';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import farmerImage from '../../../images/farmersmarket.jpeg';

const ProductsScreen = ({navigation}) => {
    const {t} = useTranslation();
    const [showAddProductDialog, setShowAddProductDialog] = useState(false);

    const onSellProduct = () => {
        navigation.navigate('Sell');
    };

    const onAddProduct = (action, product, productCategory) => {
        if (action === 'cancel') {
            setShowAddProductDialog(false);
            return;
        }

        if (!productCategory || !product) {
            // ß;
            alert(t('alert_fill_details'));
            return;
        }

        const payload = {
            type: productCategory,
            name: product,
        };

        addProductCategory(payload).then(() => setShowAddProductDialog(false));
    };

    const [expanded, setExpanded] = React.useState(false);
    const handlePress = () => setExpanded((prevState) => !prevState);

    return (
        <>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.text}>YourKrishi</Text>
                </View>
                <Card style={styles.container}>
                    <Card.Content>
                        <Image
                            style={styles.image}
                            source={require('../../../images/farmersmarket.jpeg')}
                        />
                        <View style={styles.btnWrapper}>
                            <TouchableOpacity
                                style={styles.sellItem}
                                onPress={() => onSellProduct()}>
                                <Text style={styles.textStyles}>
                                    {t('add_products')}
                                </Text>
                            </TouchableOpacity>
                            <View style={styles.sellItem}>
                                <Text style={styles.textStyles}>
                                    {t('sold_products')}
                                </Text>
                            </View>
                        </View>
                    </Card.Content>
                </Card>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        flexGrow: 1,
    },
    header: {
        backgroundColor: '#44ac5c',
        height: 100,
        paddingTop: 20,
        alignItems: 'center',
    },
    text: {
        color: Colors.white,
        fontSize: 30,
        fontStyle: 'italic',
        fontWeight: 'bold',
    },
    actionButton: {
        backgroundColor: '#f50057',
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
    sellButtonWrapper: {
        position: 'relative',
        right: 100,
        top: 100,
    },
    addButton: {
        width: '80%',
        marginLeft: '10%',
        right: 0,
        top: 20,
    },
    addNewProduct: {
        marginTop: 100,
        alignItems: 'center',
    },
    addBtn: {
        width: '50%',
        backgroundColor: '#44ac5c',
    },
    accordionStyle: {
        marginTop: 25,
    },
    btnStyles: {
        backgroundColor: '#e64c51',
    },
    image: {
        height: 190,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#E5E7EB',
    },
    addItemText: {
        fontSize: 18,
        marginBottom: 15,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    btnWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20,
    },
    sellItem: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '48%',
        height: 100,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#c7c8c9',
    },
    textStyles: {
        textAlign: 'center',
        fontWeight: 'bold',
    },
});

export default ProductsScreen;
