import React, {useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {useTranslation} from 'react-i18next';

const Logout = () => {
    const {t} = useTranslation();
    useEffect(() => {
        const triggerLogout = async () => {
            AsyncStorage.getAllKeys()
                .then((keys) => AsyncStorage.multiRemove(keys))
                .then(() => {
                    alert(t('logout_successful'));
                });
        };

        triggerLogout();
    }, [t]);

    return <></>;
};

export default Logout;
