/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Card, Divider, Title, Paragraph} from 'react-native-paper';
import {useTranslation} from 'react-i18next';
export const Product = ({product, children}) => {
    const {t} = useTranslation();
    return (
        <Card style={styles.container}>
            <Card.Content>
                <Title style={styles.name}>{t(product.name)}</Title>
                <Divider />
                <Image style={styles.productImage}
                    source={{ uri: product.image }} />
                <View style={styles.productInfo}>
                <Paragraph style={styles.info}>
                    {t('quantity')} : {product.quantity} {t('kg')}
                </Paragraph>
                <Paragraph style={styles.info}>
                    {t('price_per_kg')}: ₹ {product.price}
                </Paragraph>
                </View>
            </Card.Content>
            {children && <View style={styles.btns}>{children}</View>}
        </Card>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        marginBottom: 20,
        backgroundColor: '#F9FAFB',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#E5E7EB',
    },
    name: {
        fontSize: 24,
        textAlign: 'center',
        paddingBottom: 10,
        marginBottom: 5,
        textTransform: 'capitalize',
        fontWeight: 'bold',
        letterSpacing: 4,
    },
    info: {
        marginTop: 15,
        marginBottom: 20,
        textAlign: 'center',
        fontSize: 20,
        paddingBottom: 10,
        paddingTop: 10,
    },
    productImage: {
        height: 170,
        width: '100%',
        alignSelf: 'center',
        marginBottom: 18,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#E5E7EB',
    },
    productInfo: {
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'space-between',
        width: '100%',
    },
    btns: {
        marginTop: 20,
    },
    bold: {
        fontWeight: 'bold',
    },
});

export default Product;
